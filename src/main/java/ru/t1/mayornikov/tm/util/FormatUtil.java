package ru.t1.mayornikov.tm.util;

import static ru.t1.mayornikov.tm.constant.FormatConst.*;

public interface FormatUtil {

    static String formatBytes(long bytes) {
        if (bytes <= BYTES_K && bytes >= 0) return (bytes + " B");
        else if (bytes/ BYTES_K <= BYTES_K) return DECIMAL_FORMAT.format(bytes/ BYTES_K) + " KB";
        else if (bytes/ BYTES_M <= BYTES_K) return DECIMAL_FORMAT.format(bytes/ BYTES_M) + " MB";
        else if (bytes/ BYTES_G <= BYTES_K) return DECIMAL_FORMAT.format(bytes/ BYTES_G) + " GB";
        else if (bytes/ BYTES_T <= BYTES_K) return DECIMAL_FORMAT.format(bytes/ BYTES_T) + " TB";
        else return (bytes/ BYTES_T / BYTES_K + " PB");
    }
    
}