package ru.t1.mayornikov.tm.controller;

import ru.t1.mayornikov.tm.api.ICommandController;
import ru.t1.mayornikov.tm.api.ICommandService;
import ru.t1.mayornikov.tm.model.Command;

import static ru.t1.mayornikov.tm.util.FormatUtil.formatBytes;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("Current program arguments are not correct...");
        System.exit(1);
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        final long memoryMax = Runtime.getRuntime().maxMemory();
        final long memoryFree = Runtime.getRuntime().freeMemory();
        final long memoryUse = memoryTotal - memoryFree;
        System.out.println("PROCESSORS: " + processorCount);
        System.out.println("MAX MEMORY: " + formatBytes(memoryMax));
        System.out.println("TOTAL MEMORY: " + formatBytes(memoryTotal));
        System.out.println("USAGE MEMORY: " + formatBytes(memoryUse));
        System.out.println("FREE MEMORY: " + formatBytes(memoryFree));
    }

    @Override
    public void showErrorCommand() {
        System.out.println("[ERROR]");
        System.out.println("Current command are not correct...");
        System.out.println("Use 'help' for more information.");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.9.1");
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexey Mayornikov");
        System.out.println("E-mail: amayornikov@t1-consulting.ru");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (Command command: commandService.getCommands()) System.out.println(command);
    }

}
