# TASK MANAGER

## DEVELOPER INFO

* **Name**: Alexey Mayornikov

* **E-mail**: amayornikov@t1-consulting.ru

## SOFTWARE

* **OS**: Windows 1903 (build 18362.175)

* **JAVA**: Java 1.8.0_321

## HARDWARE

* **CPU**: i5
 
* **RAM**: 16GB

* **HDD**: 512GB

## PROGRAM BUILD

```shell
mvn clean install
```

## PROGRAM RUN

```shell
java -jar ./task-manager.jar
```